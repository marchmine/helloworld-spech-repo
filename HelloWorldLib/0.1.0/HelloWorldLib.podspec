Pod::Spec.new do |s|
  s.name             = 'HelloWorldLib'
  s.version          = '0.1.0'
  s.summary          = 'Testing upload library to cocoapod'


  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://marchmine@bitbucket.org/marchmine/helloworld-lib.git'
 
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'samboseth168@gmail.com' => 'samboseth168@gmail.com' }
  s.source           = { :git => 'https://marchmine@bitbucket.org/marchmine/helloworld-lib.git', :tag => s.version.to_s }
 

  s.ios.deployment_target = '11.0'

  s.source_files = 'HelloWorldLib/Classes/**/*'
  
  # s.resource_bundles = {
  #   'HelloWorldLib' => ['HelloWorldLib/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  
end
